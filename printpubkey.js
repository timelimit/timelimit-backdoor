const { readFileSync } = require('fs')

const publicKey = readFileSync('./publickey.pem')
  .toString('utf-8')
  .split('\n')
  .filter((line) => line.length > 0)
  .filter((line) => !line.startsWith('---'))

console.log(publicKey)

const binary = new Buffer(publicKey.join(), 'base64')

console.log(binary.toString('hex'))
