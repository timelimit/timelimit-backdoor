const { createSign, generateKeyPair } = require('crypto')
const { readFileSync } = require('fs')

const pem = readFileSync('./privatekey.pem')
const key = pem.toString('ascii')

const sign = createSign('RSA-SHA512')
sign.update('insert sequence here')
const sig = sign.sign(key, 'hex')

console.log(sig.match(/.{1,4}/g).join('-'))
