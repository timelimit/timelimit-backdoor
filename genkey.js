const { generateKeyPairSync, randomBytes } = require('crypto')
const { writeFileSync } = require('fs')

const { publicKey, privateKey } = generateKeyPairSync('rsa', {
  modulusLength: 1024,
  publicKeyEncoding: {
    type: 'spki',
    format: 'pem'
  },
  privateKeyEncoding: {
    type: 'pkcs8',
    format: 'pem'
  }
})

writeFileSync('./privatekey.pem', privateKey)
writeFileSync('./publickey.pem', publicKey)
