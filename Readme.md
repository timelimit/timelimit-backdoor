## TimeLimit Backdoor

To ensure that users don't lock out themself, there is a recovery mechanism - the backdoor.
This backdoor is a 1024 Bit RSA key. While this is weak, it's good enough to prevent a simple user from getting the public key.

Why only 1024 Bit? The key is used to sign a nonce from the App (which should be unique for an installation).
The signature must be entered by the user. Using a 1024 Bit key makes the signature *relative* small.
Then, the App knows that the support authorized the unlock.

Why asymmetric cryptography? The App is Open Source. Using something else
would allow someone else to build a unlocker tool. This prevents that.
Using a server for that is no option because this should work offline/ without internet permission too.

This repository contains 3 scripts:

- genkey.js to generate a keypair
- printpublickey.js to output the key as hex string which can be copied to the App
- sign.js to sign a nonce which allows to do a unlocking process in the App
